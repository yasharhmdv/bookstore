FROM openjdk:17-alpine

COPY ./build/libs/book-store-0.0.1-SNAPSHOT.jar /app/

CMD ["java", "-jar", "/app/bookStore-0.0.1-SNAPSHOT.jar"]
