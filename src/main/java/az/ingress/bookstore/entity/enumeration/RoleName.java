package az.ingress.bookstore.entity.enumeration;

public enum RoleName {
    AUTHOR,
    STUDENT
}
