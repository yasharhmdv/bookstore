package az.ingress.bookstore.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JWTResponseDTO {
    private String accessToken;
}
